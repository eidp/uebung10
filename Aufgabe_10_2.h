#include <iostream>

using namespace std;


class Fahrzeug {
    private:
        string kennzeichen;
        unsigned int erstzulassung;
        unsigned int hubraum;
    
    public:
        Fahrzeug(string kennzeichen, unsigned int erstzulassung, unsigned int hubraum) : kennzeichen(kennzeichen), erstzulassung(erstzulassung), hubraum(hubraum) {}

        virtual void print() {
            cout << "[Fahrzeug: " << kennzeichen << "]" << endl;
            cout << "Erstzulassung: " << erstzulassung << endl;
            cout << "Hubraum: " << hubraum << endl;
        }
};

class PKW : public Fahrzeug {
    private:
        unsigned int leistung;
        unsigned short schadstoffklasse;
    
    public:
        PKW(string kennzeichen, unsigned int erstzulassung, unsigned int hubraum, unsigned int leistung, unsigned short schadstoffklasse) : Fahrzeug(kennzeichen, erstzulassung, hubraum), leistung(leistung), schadstoffklasse(schadstoffklasse) {}

        virtual void print() {
            Fahrzeug::print();
            cout << "Leistung: " << leistung << endl;
            cout << "Schadstoffklasse: " << schadstoffklasse << endl;
        }
};

class Motorrad : public Fahrzeug {
    private:
        bool beiwagen;
    
    public:
        Motorrad(string kennzeichen, unsigned int erstzulassung, unsigned int hubraum, bool beiwagen) : Fahrzeug(kennzeichen, erstzulassung, hubraum), beiwagen(beiwagen) {}

        virtual void print() {
            Fahrzeug::print();
            cout << "Beiwagen: " << beiwagen << endl;
        }
};

class LKW : public Fahrzeug {
    private:
        unsigned short achsen;
        unsigned int zuladung;
    
    public:
        LKW(string kennzeichen, unsigned int erstzulassung, unsigned int hubraum, unsigned short achsen, unsigned int zuladung) : Fahrzeug(kennzeichen, erstzulassung, hubraum), achsen(achsen), zuladung(zuladung) {}

        virtual void print() {
            Fahrzeug::print();
            cout << "Achsen: " << achsen << endl;
            cout << "Zuladung: " << zuladung << endl;
        }
};